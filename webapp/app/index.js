'use strict';

angular.module('demoGulp', ['ngSanitize', 'angular-hal', 'ui.router', 'ui.bootstrap'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'app/main/main.html',
                controller: 'MainCtrl'
            })
            .state('people', {
                url: '/people',
                templateUrl: 'app/people/people-list.html'
            });

        $urlRouterProvider.otherwise('/');
    })
    .service('apiRootService', function (halClient) {
      var host = 'http://localhost:8080';
      return halClient.$get(host+'/api');
    })
;
