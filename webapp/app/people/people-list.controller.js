'use strict';

angular.module('demoGulp')
  .controller('PeopleCtrl', function (peopleResource, PersonForm) {
    var peopleList = this;

    var currentPage = 0;

    var updateList = function () {
      peopleResource.getList(currentPage).then(function (result) {
        peopleList.people = result.people;
        peopleList.page = result.page;
      });
    };

    peopleList.addPerson = function (form) {
      peopleResource.addPerson(peopleList.newPerson).then(function () {
        updateList();
        peopleList.newPerson = {};
        form.$setPristine();
      });
    };

    peopleList.deletePerson = function (person) {
      peopleResource.deletePerson(person).then(function () {
        updateList();
      });
    };

    peopleList.editPerson = function (person) {
      PersonForm.open(person).then(function (dataHasChanged) {
        if (dataHasChanged) {
          updateList();
        }
      });
    };

    peopleList.pageChanged = function (pageNumber) {
      currentPage = pageNumber;
      updateList();
    };

    peopleList.newPerson = {};
    updateList();
  });
