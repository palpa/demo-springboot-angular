'use strict';

angular.module('demoGulp')
  .factory('peopleResource', function (apiRootService) {
    var peopleRel = 'people';
    var embPeopleList = 'people';
    var resource = null;
    var currentPage = 0;
    var pageSize = 5;

    var peopleResource = function (reload) {
      if (resource === null || reload === true) {
        resource = apiRootService.then(function (api) {
          return api.$get(peopleRel, {'page': currentPage, 'size': pageSize});
        });
      }
      return resource;
    };


    return {
      getList: function (page) {
        currentPage = page;
        return peopleResource(true).then(function (resource) {
          if (resource.$has(embPeopleList)) {
            return resource.$get(embPeopleList).then(function (peopleList) {
              return {people: peopleList, page: resource.page};
            });
          }
          else {
            return {people: {}, page: resource.page};
          }
        });
      },
      addPerson: function (person) {
        return peopleResource(false).then(function (resource) {
            return resource.$post('self', {}, person);
          }
        );
      },
      deletePerson: function (person) {
        return person.$del('self');
      },
      editPerson: function (person, newPerson) {
        return person.$put('self', {}, newPerson);
      }
    };
  });
