'use strict';

angular.module('demoGulp')
  .factory('PersonForm', function ($modal) {

    var ModalInstanceCtrl = function ($modalInstance, readOnly, selectedPerson, peopleResource) {
      var modalForm = this;

      var dataChanged = false;

      var setReadOnly = function (value) {
        modalForm.readOnly = value;
      };

      modalForm.title = 'Edit Person';

      modalForm.submit = function () {
        peopleResource.editPerson(selectedPerson, modalForm.person).then(function () {
          dataChanged = true;
          setReadOnly(true);
          modalForm.alerts = [{type: 'success', msg: 'Edit successful'}];
        });
      };

      modalForm.reset = function () {
        modalForm.person = angular.copy(selectedPerson);
      };

      modalForm.edit = function () {
        setReadOnly(false);
        modalForm.closeAlert();
      };

      modalForm.close = function () {
        $modalInstance.close(dataChanged);
      };

      modalForm.closeAlert = function () {
        modalForm.alerts = null;
      };

      modalForm.reset();
      setReadOnly(readOnly);
    };

    return {
      open: function (person) {
        var modalInstance = $modal.open({
          templateUrl: 'app/components/people/form/person-form.html',
          controller: ModalInstanceCtrl,
          controllerAs: 'modalForm',
          backdrop: false,
          keyboard: false,
          resolve: {
            readOnly: function () {
              return false;
            },
            selectedPerson: function () {
              return person;
            }
          }
        });

        return modalInstance.result.then(function (dataChanged) {
          return dataChanged;
        }, function (reason) {
          console.info('Modal dismissed at: ' + new Date() + ', ' + reason);
          return false;
        });
      }
    };
  });
