'use strict';

angular.module('demoGulp')
  .directive('paginationControl', function () {
    return {
      restrict: 'E',
      scope: {
        page: '=',
        pageChanged: '&onPageChange'
      },
      template: '<div ng-hide="page.totalPages <= 1"> ' +
      ' <pagination total-items="page.totalElements" ng-model="currentPage" max-size="10" items-per-page="page.size" class="pagination-sm" boundary-links="true" ng-change="pageChanged({pageNumber:(currentPage - 1)})" previous-text="<" next-text=">" first-text="<<" last-text=">>"></pagination> ' +
      '</div>'
    };
  });
